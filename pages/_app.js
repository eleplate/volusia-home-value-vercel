import '../styles/globals.css'
import React from 'react';
import { render } from 'react-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { useEffect } from 'react'
import { useRouter } from 'next/router'

import * as gtag from '../lib/gtag'

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: '#0070f3'
    }
  }
});

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  useEffect(() => {
    const handleRouteChange = (url) => {
      gtag.pageview(url)
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])
  return(
    
    <MuiThemeProvider theme={theme}>
        <Component {...pageProps} />
    </MuiThemeProvider>
    
  )
  
  
}
export default MyApp;

