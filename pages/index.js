import Head from 'next/head'
import Button from '@material-ui/core/Button'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import React, { useState, useEffect, useRef } from "react";
// import SearchLocationInput from '../components/searchLocationInput.js';
import test from '../components/test.js';
import Modal from '../components/modal.js';

import AddressForm from '../components/addressForm';
import Footer from '../components/footer.js';



export default function Home() {

const [addressCapture, setAddressCapture] = useState(true)
const [query, setQuery] = useState("");
  let autoComplete;

const loadScript = (url, callback) => {
  let script = document.createElement("script");
  script.type = "text/javascript";

  if (script.readyState) {
    script.onreadystatechange = function() {
      if (script.readyState === "loaded" || script.readyState === "complete") {
        script.onreadystatechange = null;
        callback();
      }
    };
  } else {
    script.onload = () => callback();
  }

  script.src = url;
  document.getElementsByTagName("head")[0].appendChild(script);
};

function handleScriptLoad(updateQuery, autoCompleteRef) {
  autoComplete = new window.google.maps.places.Autocomplete(
    autoCompleteRef.current,
    { types: ["address"], componentRestrictions: { country: "us" } }
  );
  autoComplete.setFields(["address_components", "formatted_address"]);
  autoComplete.addListener("place_changed", () =>
    handlePlaceSelect(updateQuery)
  );
}

async function handlePlaceSelect(updateQuery) {
  const addressObject = autoComplete.getPlace();
  const query = addressObject.formatted_address;
  updateQuery(query);
  console.log(addressObject);
}

  
const autoCompleteRef = useRef(null);

useEffect(() => {
  loadScript(
    `https://maps.googleapis.com/maps/api/js?key=AIzaSyDvxRBtOe_OGOBa1lluHmfg4JHaGy8MKUQ&libraries=places`,
    () => handleScriptLoad(setQuery, autoCompleteRef)
  );
}, []);

function captureAddress() {
  //event.preventDefault();
  console.log(query)
  setAddressCapture(false)
}

if (addressCapture) {


  return (
    <div className={styles.container}>
      <Head>
        <title>Volusia Home Value</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <br />
        </h1>
        <h1 className={styles.colorTitle}>
          Volusia Home Value
        </h1>

        <p className={styles.description}>
          How much is your home worth? 
          <br />
          <span className={styles.colorDescription}>Find out for free.</span> Enter your address:{' '}
          {/* <code className={styles.code}>your address</code> */}
        </p>
        <form onSubmit={captureAddress}>
        <div className={styles.searchLocationInput}>
      <input
        ref={autoCompleteRef}
        onChange={event => setQuery(event.target.value)}
        placeholder="Enter your address..."
        value={query}
      />
    </div>
        
        <p />
        {/* <Link href="/form" passHref> */}
          <div className={styles.centeredDiv}>
          <Button type="submit" variant="contained" className={styles.btnGrad}>Submit</Button>
          </div>
        {/* </Link> */}
       
        </form>

        {/* <div className={styles.grid}>
          <a href="/about" className={styles.card}>
            <h3>About &rarr;</h3>
            <p>Learn how Volusia Home Value is the best in the business.</p>
          </a>

          <a href="/contact" className={styles.card}>
            <h3>Contact &rarr;</h3>
            <p>Have questions or want to speak with someone on our team?</p>
          </a>
        </div> */}
      </main>

      <Footer />
    </div>
  )
} else {
  return (
    <div className={styles.container}>
      <Head>
        <title>Volusia Home Value</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.colorTitle}>
          Almost there!
        </h1>

        <p className={styles.description}>
          {/* Address: (insert address) */}
          <br />
          Enter your information:{' '}
          {/* <code className={styles.code}>your address</code> */}
        </p>
        <AddressForm address={query}/>

        <p className={styles.description}>
          {/* <span className={styles.description}>
              Your request will be forwarded to a local real estate agent, who will 
              <br />email a customized market report for your property within 24 hours!
            </span> */}
          {/* <code className={styles.code}>your address</code> */}
        </p>

        {/* <div className={styles.grid}>
          <a href="/about" className={styles.card}>
            <h3>About &rarr;</h3>
            <p>Learn how Volusia Home Value is the best in the business.</p>
          </a>

          <a href="/contact" className={styles.card}>
            <h3>Contact &rarr;</h3>
            <p>Have questions or want to speak with someone on our team?</p>
          </a>
        </div> */}
        <p className={styles.disclaimer}>
          By submitting your information you are agreeing to be contacted by volusiahomevalue.com regarding real estate. 
          We will not utilize your information for any other purpose or sell your information to third parties 
        </p>
        

    </main>

    <Footer />
    </div>
  )
}



} 
