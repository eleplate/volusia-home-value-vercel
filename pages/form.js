import Head from 'next/head'
import Button from '@material-ui/core/Button'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import React, { useState, useEffect, useRef } from "react";
import SearchLocationInput from '../components/searchLocationInput.js';
import test from '../components/test.js';
import Modal from '../components/modal.js';
import AddressForm from '../components/addressForm';
import Footer from '../components/footer.js';



export default function Home() {


  return (
    <div className={styles.container}>
      <Head>
        <title>Volusia Home Value</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.colorTitle}>
          Almost there!
        </h1>

        <p className={styles.description}>
          {/* Address: (insert address) */}
          <br />
          Enter your information:{' '}
          {/* <code className={styles.code}>your address</code> */}
        </p>
        <AddressForm />

        <p className={styles.description}>
          {/* <span className={styles.description}>
              Your request will be forwarded to a local real estate agent, who will 
              <br />email a customized market report for your property within 24 hours!
            </span> */}
          {/* <code className={styles.code}>your address</code> */}
        </p>

        <div className={styles.grid}>
          <a href="/about" className={styles.card}>
            <h3>About &rarr;</h3>
            <p>Learn how Volusia Home Value is the best in the business.</p>
          </a>

          <a href="/contact" className={styles.card}>
            <h3>Contact &rarr;</h3>
            <p>Have questions or want to speak with someone on our team?</p>
          </a>
        </div>
        

    </main>

    <Footer />
    </div>
  )
}
