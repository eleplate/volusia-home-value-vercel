import Head from 'next/head'
import Button from '@material-ui/core/Button'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import React, { useState, useEffect, useRef } from "react";
import SearchLocationInput from '../components/searchLocationInput.js';
import test from '../components/test.js';
import Modal from '../components/modal.js';
import Footer from '../components/footer.js';



export default function About() {


  return (
    <div className={styles.container}>
      <Head>
        <title>Volusia Home Value</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          About <br />
        </h1>
        <h1 className={styles.colorTitle}>
          Volusia Home Value
        </h1>

        <p className={styles.aboutText}>
        Instead of an online tool which is only a vague estimate, 
        volusiahomevalue.com provides a local expert who generates a customized 
        market report for your property, emailed directly to you, including comparable sales.
        <p />
        Rather meet in person for a 
        more accurate evaluation? Not a problem. Click the contact box and set up a no 
        obligation listing consultation with a local real estate agent!<p/> Once the agent 
        has seen the ins and outs of your home they will be able to provide the most 
        accurate pricing information and answer any other questions you may have about 
        the listing process. We would love to meet with you!  
        </p>
      
        <div className={styles.grid}>
          <a href="/" className={styles.card}>
            <h3>Home &rarr;</h3>
            <p>Learn the value of your home for free by entering your address.</p>
          </a>

          <a href="/contact" className={styles.card}>
            <h3>Contact &rarr;</h3>
            <p>Have questions or want to speak with someone on our team?</p>
          </a>
        </div>
      </main>
      <Footer />
      
    </div>
  )
}
