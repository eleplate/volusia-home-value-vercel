import React, {useState} from "react";
import axios from 'axios';

import styles from '../styles/Home.module.css'
import Button from '@material-ui/core/Button'



export default (props) => {
    const [status, setStatus] = useState({
      submitted: false,
      submitting: false,
      info: { error: false, msg: null },
    })
    const [inputs, setInputs] = useState({
      name: '',
      email: '',
      number: '',
      address: props.address
    })
    const handleServerResponse = (ok, msg) => {
      if (ok) {
        setStatus({
          submitted: true,
          submitting: false,
          info: { error: false, msg: msg },
        })
        setInputs({
          name: '',
          email: '',
          number: '',
          address: props.address
        })
      } else {
        setStatus({
          info: { error: true, msg: msg },
        })
      }
    }
    const handleOnChange = (e) => {
      e.persist()
      setInputs((prev) => ({
        ...prev,
        [e.target.id]: e.target.value,
      }))
      setStatus({
        submitted: false,
        submitting: false,
        info: { error: false, msg: null },
      })
    }
    const handleOnSubmit = (e) => {
      e.preventDefault()
      setStatus((prevStatus) => ({ ...prevStatus, submitting: true }))
      axios({
        method: 'POST',
        url: 'https://formspree.io/f/mpzonjoe',
        data: inputs,
      })
        .then((response) => {
          handleServerResponse(
            true,
            'All set! Your report will be emailed to you within 24 hours!'
          )
        })
        .catch((error) => {
          handleServerResponse(false, error.response.data.error)
        })
    }

    <div className={styles.searchLocationInput}>
//             <input type="name" placeholder='Name' />
//         </div>
//         <br />
//         <div className={styles.searchLocationInput}>
//             <input type="email" placeholder="Email" />
//         </div>
//         <br />
//         <div className={styles.searchLocationInput}>
//             <input type="text" placeholder='How can we help?' />
//         </div>
//         <p />
//         <div className={styles.centeredDiv}>
//             {status === "SUCCESS" ? <p>Thanks!</p> : <Button variant="contained" className={styles.btnGrad} color="secondary">Submit</Button>}
//             {status === "ERROR" && <p>Ooops! There was an error.</p>}
//         </div>
    return (
      <main>
        <form onSubmit={handleOnSubmit}>
          <div className={styles.searchLocationInput}>
            
            <input
                id="name"
                type="text"
                //name="_replyto"
                onChange={handleOnChange}
                placeholder="Name"
                value={inputs.name}
            />
          </div>
          <br />
          <div className={styles.searchLocationInput}>
            <input
                id="email"
                type="email"
                //name="_replyto"
                onChange={handleOnChange}
                required
                placeholder="Email (where your report will be sent)"
                value={inputs.email}
            />
          </div>
          <br />
          <div className={styles.searchLocationInput}>
            <input 
                id="number"
                name="number"
                onChange={handleOnChange}
                type="text"
                placeholder="Phone number (optional)"
                value={inputs.number}
            />
          </div>
            <p />
            <div className={styles.centeredDiv}>
                <Button variant="contained" className={styles.btnGrad} color="secondary" type="submit" disabled={status.submitting}>
                    {!status.submitting
                    ? !status.submitted
                        ? 'Submit'
                        : 'Submitted'
                    : 'Submitting...'}
                </Button>
            </div>
        </form>
        {status.info.error && (
          <div className="error">Error: {status.info.msg}</div>
        )}
        {!status.info.error && status.info.msg && <p>{status.info.msg}</p>}
      </main>
    )
  }

// export default class ContactForm extends React.Component {
//   constructor(props) {
//     super(props);
//     this.submitForm = this.submitForm.bind(this);
//     this.state = {
//       status: ""
//     };
//   }

//   render() {
//     const { status } = this.state;
//     return (
//       <form
//         onSubmit={this.submitForm}
//         action="https://formspree.io/f/xnqorboj"
//         method="POST"
//       >
//         {/* <!-- add your custom form HTML here --> */}
//         <div className={styles.searchLocationInput}>
//             <input type="name" placeholder='Name' />
//         </div>
//         <br />
//         <div className={styles.searchLocationInput}>
//             <input type="email" placeholder="Email" />
//         </div>
//         <br />
//         <div className={styles.searchLocationInput}>
//             <input type="text" placeholder='How can we help?' />
//         </div>
//         <p />
//         <div className={styles.centeredDiv}>
//             {status === "SUCCESS" ? <p>Thanks!</p> : <Button variant="contained" className={styles.btnGrad} color="secondary">Submit</Button>}
//             {status === "ERROR" && <p>Ooops! There was an error.</p>}
//         </div>
//       </form>
//     );
//   }

//   submitForm(ev) {
//     ev.preventDefault();
//     const form = ev.target;
//     const data = new FormData(form);
//     const xhr = new XMLHttpRequest();
//     xhr.open(form.method, form.action);
//     xhr.setRequestHeader("Accept", "application/json");
//     xhr.onreadystatechange = () => {
//       if (xhr.readyState !== XMLHttpRequest.DONE) return;
//       if (xhr.status === 200) {
//         form.reset();
//         this.setState({ status: "SUCCESS" });
//       } else {
//         this.setState({ status: "ERROR" });
//       }
//     };
//     xhr.send(data);
//   }
// }